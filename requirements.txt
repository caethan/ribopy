numpy==1.9.2
scipy==0.16.0
pandas==0.16.2
cython==0.23
logging==0.4.9.6