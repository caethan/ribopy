################################################################################
#
# RiboPy, a statistical package for analysis of translational efficiency
# from gene counts of RNA-Seq and ribosome profiling experiments
#
################################################################################

# Package imports
from __future__ import division
import numpy as np
import pandas as pd
import logging

import math # For fsum, a stable summation function
from scipy.stats import poisson, nbinom, norm, chi2
from scipy.special import gammaln #Natural log of the gamma function (gamma(k+1)=k!)
from statsmodels.api import OLS

# Load in the Cython pvalue calculations (too slow otherwise!)

# Setup Cython importing to include the numpy headers so Cython can work with
# numpy arrays
import pyximport
pyximport.install(setup_args={'include_dirs': np.get_include()})
import pval_calc

# Setup a logger
logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)

################################################################################
# General function definitions
################################################################################

def poisson_logpmf(means, counts):
    """Calculates the logarithm of the probabilities of Poisson distributions 
    with given means and counts.
    """
    return counts * np.log(means) - means - gammaln(counts + 1)
    
def poisson_multi_prob(means, counts, axis=None):
    """Calculates the product of multiple Poisson probabilities given Poisson
    mean parameters and count values for each Poisson variable.
    """
    return np.exp(poisson_logpmf(means, counts).sum(axis=axis))
    
def scaling_constant(muA, muB, k):
    return np.exp(k * np.log(muA + muB) - gammaln(k + 1) - muA - muB)

################################################################################
# Classes
################################################################################

class RiboError(Exception):
    """Base class for general exceptions in this module."""
    pass

class GeneCounts(object):
    
    def __init__(self, counts, conditions, methods, sample_ids=None):
        """Provide the initial data from the experiments to analyze:
        counts - an N by M array of gene counts, where N indexes the genes and
            M indexes the samples
        
        conditions - an M-length array of integers indicating the experiment
            conditions, with 0 indicating a control sample and 1 the treatment
            sample.
            
        methods - an M-length array of integers indicating which experimental
            method was used for each sample.  0 indicates mRNA transcript 
            counts, while 1 indicates ribosomal footprint counts.
        
        (Both of these arrays use integers rather than booleans 
        for potential extension later.)
        """
        self.N, self.M = counts.shape
        
        if sample_ids is None:
            self.sample_ids = np.array(['Sample %i' % (i + 1) for i in range(self.M)])
        else:
            self.sample_ids = sample_ids
        if len(self.sample_ids) != self.M:
            raise RiboError('Sample IDs do not match count data.')
        
        #Check input data
        if len(conditions) != self.M or len(methods) != self.M:
            raise RiboError('Conditions or methods provided do not match count data.')

        test = np.unique(conditions)
        if not (test == np.array([0, 1])).all():
            raise RiboError('Invalid condition ids provided.')
            
        test = np.unique(methods)
        if not (test == np.array([0, 1])).all():
            raise RiboError('Invalid method ids provided.')
        
        #Transform the data to two pandas dataframes
        self.gene_data = pd.DataFrame(counts, columns=self.sample_ids)
        cond_map = {0:'Control', 1:'Treatment'}
        meth_map = {0:'mRNA', 1:'Footprint'}
        
        self.sample_data = pd.DataFrame()
        self.sample_data['Conditions'] = map(lambda x: cond_map[x], conditions)
        self.sample_data['Methods'] = map(lambda x: meth_map[x], methods)
        self.sample_data.index = self.sample_ids

################################################################################
#   Utility functions
################################################################################

    def select_samples(self, kwds):
        valid = np.ones(self.M, dtype=bool)
        for key, value in kwds.iteritems():
            valid &= self.sample_data[key] == value
        return self.sample_ids[valid]

################################################################################
#   Parameter estimation functions
################################################################################

    def estimate_size_factors(self, separate=True):
        """Take the provided count data and estimate size factors for
        each of all M samples.  
        
        The estimate is the median across all genes
        of the ratio of each gene's count to the pseudo-reference for that
        gene and sample. The pseudo-reference is the geometric average of 
        counts, where samples from different methods are pooled to produce their
        own pseudo-references.
        
        We then normalize each pool of size factors so that their mean is 1.
        """
        self.sample_data['Size Factors'] = 0.0
        
        if separate:
            #Normalize mRNA and RPF samples separately   
            for method in np.unique(self.sample_data['Methods']):
                samples = self.select_samples({'Methods':method})
                counts = self.gene_data[samples]
                #Find genes were we've had no reads in any of our samples so we 
                #can exclude them from our median calculation                
                counts = counts[(counts == 0).all(axis=1) == False]
                #Adjust for remaining zero counts by increasing all counts by 1 
                #and then subtracting 1 after taking the geometric mean
                pseudoref = np.exp(np.log(counts + 1).mean(axis=1)) - 1
                #Go through each sample and estimate its size factor with this 
                #pseudoreference
                for sample in samples:
                    self.sample_data.loc[sample, 'Size Factors'] = (counts[sample] / pseudoref).median()
            for method in np.unique(self.sample_data['Methods']):
                samples = self.select_samples({'Methods':method})
                self.sample_data.loc[samples, 'Size Factors'] /= self.sample_data.loc[samples, 'Size Factors'].mean()
        else:
            #Normalize all samples together
            counts = self.gene_data
            counts = counts[(counts == 0).all(axis=1) == False]
            pseudoref = np.exp(np.log(counts + 1).mean(axis=1)) - 1
            for sample in self.sample_ids:
                self.sample_data.loc[sample, 'Size Factors'] = (counts[sample] / pseudoref).median()
            self.sample_data['Size Factors'] /= self.sample_data['Size Factors'].mean()
        
        #Check to make sure we've set all the size factors:
        if not (self.sample_data['Size Factors'] > 0).all():
            raise RiboError('One or more size factors was not calculated.')

    def estimate_expression_strength(self):
        """Use count data to calculate initial estimates of base expression 
        strength for each gene under each different experimental condition and method.
        
        Simply the average of all size-adjusted counts for each condition and method.
        """
        for condition in np.unique(self.sample_data['Conditions']):
            for method in np.unique(self.sample_data['Methods']):
                samples = self.select_samples({'Methods':method, 'Conditions':condition})
                if len(samples) <= 0:
                    raise RiboError('No data was found for some condition/method.')
                self.gene_data['Expression, %s - %s' % (method, condition)] = (self.gene_data[samples] / self.sample_data.loc[samples, 'Size Factors']).mean(axis=1)

    def estimate_expression_variance(self):
        """Raw estimation of expression variance using sample data for each
        gene under every method/condition.  If only one sample is available 
        for any method/condition, raise an error.
        """
        for condition in np.unique(self.sample_data['Conditions']):
            for method in np.unique(self.sample_data['Methods']):
                samples = self.select_samples({'Methods':method, 'Conditions':condition})
                if len(samples) <= 1:
                    raise RiboError('Insufficient data found for variance estimation.')
                raw =  self.gene_data[samples] / self.sample_data.loc[samples, 'Size Factors']
                pooled = self.gene_data['Expression, %s - %s' % (method, condition)]
                self.gene_data['Variance, %s - %s' % (method, condition)] = (raw.sub(pooled, axis=0) ** 2).sum(axis=1) / (len(samples) - 1)
                # Bias correction
                correction = pooled * (1 / self.sample_data.loc[samples, 'Size Factors']).sum() / len(samples)
                self.gene_data['Variance, %s - %s' % (method, condition)] -= correction  

    def estimate_dispersion(self, calc="Poisson"):
        """Construct a fit of raw variance versus base expression strength over all genes for
        each method/condition to estimate dispersion.
        
        Poisson:  variance is equal to base expression strength.  Equivalent to a dispersion of 0.  Not generally recommended!
        OLS: ordinary least-squares fit with constant dispersion.
        MLE: maximum likelihood estimation using a fixed-point method
        """
        
        def G_mle(alpha, mu, counts):
            """This is the fixed point for the maximum likelihood estimate of alpha.
            
            alpha - float in (0, inf), the dispersion parameter
            mu - float, the fitted mean of the sample data
            counts - array of ints of length N, where N is the number of samples
    
            returns a single float that is a new estimate of alpha
            """
            def subfunc(k, alpha):
                v = np.arange(k)
                return ((alpha * v) / (1 + alpha * v)).sum()
    
            return np.log(1 + alpha * mu) / (mu - np.array(map(subfunc, counts, [alpha] * len(counts))).mean())
        
        def G_map(alpha, mu, eta, counts):
            """Fixed point for MAP estimate of alpha (exponential prior on 1/alpha)"""
            def subfunc(k, alpha):
                v = np.arange(k)
                return ((alpha * v) / (1 + alpha * v)).sum()

            return (eta + np.log(1 + alpha * mu)) / (mu - np.array(map(subfunc, counts, [alpha] * len(counts))).mean())
        
        def MLE_dispersion(counts, maxiter=10000, epsilon=1e-6):
            if (counts <= 0).all():
                return None
            alpha_min = 1 / 13.0
            N = len(counts)
            mu = counts.mean()
            var = counts.std() ** 2
            if var <= mu:
                #return 0.0
                return alpha_min
            alpha = N / (counts >= 1).sum() - 1 / mu
            for i in range(maxiter):
                new = G_mle(alpha, mu, counts)
                if 2 * abs(new - alpha) / (new + alpha) < 1e-6:
                    break
                alpha = new
            return max(alpha_min, new)

        def MAP_dispersion(counts, maxiter=10000, epsilon=1e-6):
            if (counts <= 0).all():
                return None
            alpha_min = 1 / 13.0
            N = len(counts)
            mu = counts.mean()
            eta = 2.5 / (1 + mu)
            alpha = N / (counts >= 1).sum() - 1 / mu
            for i in range(maxiter):
                new = G_map(alpha, mu, eta, counts)
                if 2 * abs(new - alpha) / (new + alpha) < 1e-6:
                    break
                alpha = new
            #return max(alpha_min, new)
            return new
            
        for condition in np.unique(self.sample_data['Conditions']):
            for method in np.unique(self.sample_data['Methods']):
                if calc == "Poisson":
                    self.gene_data['Dispersion, %s - %s' % (method, condition)] = 0.0
                elif calc == "OLS":
                    x = self.gene_data['Expression, %s - %s' % (method, condition)]
                    y = self.gene_data['Variance, %s - %s' % (method, condition)]
                    # Fit to a model of y = x + alpha * x^2
                    model = OLS(y - x, x ** 2)
                    results = model.fit()
                    self.gene_data['Dispersion, %s - %s' % (method, condition)] = results.params[0]
                elif calc == "GLM":
                    raise NotImplementedError("Generalized linear modeling is not yet implemented.")
                elif calc == "MLE":
                    samples = self.select_samples({'Methods':method, 'Conditions':condition})
                    counts = np.array(self.gene_data[samples] / self.sample_data.loc[samples, 'Size Factors'])
                    self.gene_data['Dispersion, %s - %s' % (method, condition)] = 0.0
                    for i in range(len(counts)):
                        self.gene_data.loc[i, 'Dispersion, %s - %s' % (method, condition)] = MLE_dispersion(counts[i])
                elif calc == "MAP":
                    samples = self.select_samples({'Methods':method, 'Conditions':condition})
                    counts = np.array(self.gene_data[samples] / self.sample_data.loc[samples, 'Size Factors'])
                    self.gene_data['Dispersion, %s - %s' % (method, condition)] = 0.0
                    for i in range(len(counts)):
                        self.gene_data.loc[i, 'Dispersion, %s - %s' % (method, condition)] = MAP_dispersion(counts[i])                    
                else:
                    raise RiboError("Invalid calculation method provided for dispersion estimates")
                    
                    
    def estimate_trans_efficiency(self):
        """Estimate of the pooled translation efficiency parameters 
        using the current values of the expression strength parameters, in 
        combination with the count data.
        """
        
        ctrl_rna = self.select_samples({'Methods':'mRNA', 'Conditions':'Control'})
        ctrl_rpf = self.select_samples({'Methods':'Footprint', 'Conditions':'Control'})
        treat_rna = self.select_samples({'Methods':'mRNA', 'Conditions':'Treatment'})
        treat_rpf = self.select_samples({'Methods':'Footprint', 'Conditions':'Treatment'})
        
        # Only work with those genes where we have at least one count for each method/condition
        valid = (self.gene_data[ctrl_rna].sum(axis=1) > 0) & \
                (self.gene_data[ctrl_rpf].sum(axis=1) > 0) & \
                (self.gene_data[treat_rna].sum(axis=1) > 0) & \
                (self.gene_data[treat_rpf].sum(axis=1) > 0)
        
        alpha = self.gene_data.loc[valid, 'Dispersion, mRNA - Control']
        control = self.gene_data.loc[valid, 'Expression, Footprint - Control'] / (1 + (1 + alpha) * self.gene_data.loc[valid, 'Expression, mRNA - Control'])
        
        alpha = self.gene_data.loc[valid, 'Dispersion, mRNA - Treatment']
        treatment = self.gene_data.loc[valid, 'Expression, Footprint - Treatment'] / (1 + (1 + alpha) * self.gene_data.loc[valid, 'Expression, mRNA - Treatment'])
                
        # Finally, we pool the data from control and treatment groups
        self.gene_data['Translation Efficiency - Pooled'] = None
        self.gene_data['Translation Efficiency - Control'] = None
        self.gene_data['Translation Efficiency - Treatment'] = None
        # TODO: pool adjusted by sample sizes
        self.gene_data.loc[valid, 'Translation Efficiency - Pooled'] = (control + treatment) / 2
        self.gene_data.loc[valid, 'Translation Efficiency - Control'] = control
        self.gene_data.loc[valid, 'Translation Efficiency - Treatment'] = treatment

    def estimate_pooled_means(self):
        """Use the pooled translation efficiencies, expression strengths, and size factors
        to calculate pooled means for the total counts under each condition and method.
        """
        for condition in np.unique(self.sample_data['Conditions']):
            samples = self.select_samples({'Methods':'mRNA', 'Conditions':condition})
            self.gene_data['Mean, %s - %s' % ('mRNA', condition)] = self.gene_data['Expression, mRNA - %s' % condition] *  self.sample_data.loc[samples, "Size Factors"].sum()
            
            samples = self.select_samples({'Methods':'Footprint', 'Conditions':condition})
            self.gene_data['Mean, %s - %s' % ('Footprint', condition)] = self.gene_data['Expression, mRNA - %s' % condition] *  self.sample_data.loc[samples, "Size Factors"].sum() * self.gene_data['Translation Efficiency - Pooled']

################################################################################
#   P-value calculation functions
################################################################################

    def calc_pvalues(self, pval_calc_func, mincount=10, badvals=None):
        """Calculate p-values for statistically significant differences in 
        translational efficiency between control and treatment samples in each 
        gene.  Simply loops through each gene, discarding genes with any counts 
        below mincount or that are provided in badvals, 
        and using the provided pval_calc_func to calculate the 
        actual p-values. Various options for pval_calc_func are provided below 
        this function using different approaches, or new functions can be 
        provided.  These functions must take the parameters and 
        pooled count data for a single gene, ordered as 
        (RNA_control, RNA_treatment, RPF_ctrl, RPF_treatment).
        """
        self.gene_data['P-value'] = None
        
        ctrl_rna = self.select_samples({'Methods':'mRNA', 'Conditions':'Control'})
        ctrl_rpf = self.select_samples({'Methods':'Footprint', 'Conditions':'Control'})
        treat_rna = self.select_samples({'Methods':'mRNA', 'Conditions':'Treatment'})
        treat_rpf = self.select_samples({'Methods':'Footprint', 'Conditions':'Treatment'})
        
        # Swap over to numpy arrays so we can pass these into Cython
        methods = np.array([0, 0, 1, 1])
        conditions = np.array([0, 1, 0, 1])
        
        mu = np.empty([self.N, 4])
        mu[:, 0] = self.gene_data['Mean, mRNA - Control']
        mu[:, 1] = self.gene_data['Mean, mRNA - Treatment']
        mu[:, 2] = self.gene_data['Mean, Footprint - Control']
        mu[:, 3] = self.gene_data['Mean, Footprint - Treatment']
        
        alpha = np.empty([self.N, 4])
        alpha[:, 0] = self.gene_data['Dispersion, mRNA - Control']
        alpha[:, 1] = self.gene_data['Dispersion, mRNA - Treatment']
        alpha[:, 2] = self.gene_data['Dispersion, Footprint - Control']
        alpha[:, 3] = self.gene_data['Dispersion, Footprint - Treatment']        
                
        for i in range(self.N):
            counts = np.array([self.gene_data.loc[i, ctrl_rna].sum(), self.gene_data.loc[i, treat_rna].sum(),
                               self.gene_data.loc[i, ctrl_rpf].sum(), self.gene_data.loc[i, treat_rpf].sum()], dtype=int)
            # Skip any genes with any sample counts less than mincount or in badvals
            if (counts < mincount).any():
                continue
            if badvals is not None and i in badvals:
                continue
            self.gene_data.loc[i, 'P-value'] = pval_calc_func(counts, conditions, methods, alpha[i], mu[i])

################################################################################
# p-value calculation methods (note not inside the class)
################################################################################

def calc_pvalue(counts, conditions, methods, alpha, mu):
    """This function just wraps calls to the Cython function that actually
    calculate the pvalues we're interested in.
    """
    return pval_calc.calc_pvalue(counts, conditions, methods, alpha, mu)

################################################################################
# Main execution path
################################################################################

def _main():
    """Main execution path for RiboPy when run as a script.
    
    Currently does nothing.
    """
    pass

if __name__ == "__main__":
    _main()