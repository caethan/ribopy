import numpy as np
cimport numpy as np

from scipy.stats import poisson, nbinom

cdef np.ndarray[np.float64_t, ndim=1] sub_prob_quad(double threshold,
                  int rna_count,
                  int rpf_count,
                  np.ndarray[np.float64_t, ndim=1] rna_probs_ctrl,
                  np.ndarray[np.float64_t, ndim=1] rna_probs_treat,
                  np.ndarray[np.float64_t, ndim=1] rpf_probs_ctrl,
                  np.ndarray[np.float64_t, ndim=1] rpf_probs_treat):
    """Cython-only sub-function that takes the independent probability distributions for each
    pooled count over mRNA and footprint samples and a probability threshold 
    estimated from the observed counts, and explicitly calculates the 
    total tail probability - i.e., the total joint probability of observing count sums
    less likely than that actually seen.  
    """
    
    cdef double total_prob, frac_prob, value, rpf_sum, rna_sum, rpf_max, rna_max
    cdef int a, ap, i, rpf_low, rpf_up, rna_low, rna_up
    cdef np.ndarray[np.float64_t, ndim=1] rna_probs, rpf_probs

    # Look at the joint probability of a particular division given a total count over both control and treatment
    rna_probs = rna_probs_ctrl[:rna_count+1] * rna_probs_treat[:rna_count+1][::-1]
    rpf_probs = rpf_probs_ctrl[:rpf_count+1] * rpf_probs_treat[:rpf_count+1][::-1]

    # Improve performance by doing a full examination of tail probabilities only in regions where we might be 
    # above the threshold (by far most of the search space is below the threshold for nearly all cases)
    rna_max = rna_probs.max()
    rpf_max = rpf_probs.max()

    if rpf_max <= 0.0 or rna_max <= 0.0:
        return np.array([0.0, 0.0], dtype=float)
        
    rna_low = 0
    rna_hi = 0
    for i in range(rna_count + 1):
       if rna_probs[i] >= threshold / rpf_max:
            if rna_low == 0:
                rna_low = i
            else:
                rna_hi = i+1 #Plus one for the range input
    rpf_low = 0
    rpf_hi = 0
    for i in range(rpf_count + 1):
        if rpf_probs[i] >= threshold / rna_max:
            if rpf_low == 0:
                rpf_low = i
            else:
                rpf_hi = i+1 #Plus one for the range input
    
    total_prob = rpf_probs.sum() * rna_probs.sum()
    frac_prob = total_prob
    
    # Short-circuit when we've got bad data - I know what the answer will be!
    if total_prob == 0.0:
        return np.array([0.0, 0.0])
    
    for a in range(rna_low, rna_hi):
        for ap in range(rpf_low, rpf_hi):
            value = rna_probs[a] * rpf_probs[ap]
            if value > threshold:
                frac_prob -= value
                
    return np.array([frac_prob, total_prob], dtype=float)

def calc_pvalue(np.ndarray[np.long_t, ndim=1] counts,
                           np.ndarray[np.long_t, ndim=1] conditions,
                           np.ndarray[np.long_t, ndim=1] methods,
                           np.ndarray[np.float64_t, ndim=1] alpha_params,
                           np.ndarray[np.float64_t, ndim=1] mu_params):
    """Take the raw count data along with method and condition information and estimated
    dispersion and mean parameters, calculate probability distributions (NB for alpha > 0, Poisson
    for alpha = 0) and estimate p-values for the observed counts under the null hypothesis of no 
    difference in translation efficiency between conditions.
    
    Runs for a single gene at a time!
    """
    
    cdef double threshold, f, t
    cdef int rna_count, rpf_count, total_count, i, k_A, k_B, k_Aprime, k_Bprime
    cdef np.ndarray[np.long_t, ndim=1] idx
    cdef np.ndarray[np.float64_t, ndim=1] rna_probs_ctrl, rpf_probs_ctrl, rna_probs_treat, rpf_probs_treat, vals
    
    total_count = counts.sum()
    
    # Methods: 0 = mRNA, 1 = RPF
    # Conditions: 0 = Control, 1 = Treatment
    k_A = counts[(methods == 0) & (conditions == 0)].sum()
    k_B = counts[(methods == 0) & (conditions == 1)].sum()
    k_Aprime = counts[(methods == 1) & (conditions == 0)].sum()
    k_Bprime = counts[(methods == 1) & (conditions == 1)].sum()
    
    rna_count = k_A + k_B
    rpf_count = k_Aprime + k_Bprime
    
    idx = np.arange(total_count+1)
        
    #For the probabilities, if alpha is 0, we use a poisson.  Otherwise, it's NB.
    probs = []
    for i in range(4):
        if alpha_params[i] > 0:
            probs.append(nbinom(1 / alpha_params[i], 1 / (1 + alpha_params[i] * mu_params[i])).pmf(idx))
        else:
            probs.append(poisson(mu_params[i]).pmf(idx))
    rna_probs_ctrl = probs[0][:rna_count+1]
    rna_probs_treat = probs[1][:rna_count+1]
    rpf_probs_ctrl = probs[2][:rpf_count+1]
    rpf_probs_treat = probs[3][:rpf_count+1]
    
    threshold = rna_probs_ctrl[k_A] * rna_probs_treat[k_B] * \
                rpf_probs_ctrl[k_Aprime] * rpf_probs_treat[k_Bprime]

    vals = sub_prob_quad(threshold, rna_count, rpf_count,
                         rna_probs_ctrl, rna_probs_treat, rpf_probs_ctrl, rpf_probs_treat)
                      
    if vals[1] == 0.0:
        # Warning: the current method fails badly for "large" counts because the probability distributions
        # become sufficiently spread out that the joint distributions get numerically rounded to zero.  
        # TODO: Needs to be fixed!
        return None
        # raise ValueError('Invalid result from p-value calculation: areas %s, counts %s' % 
        #     (str(vals), ','.join(map(lambda x: str(x), [k_A, k_B, k_Aprime, k_Bprime]))))
    return vals[0] / vals[1]