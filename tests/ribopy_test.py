from unittest import TestCase

import ribopy

import logging

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)

class TestObject(TestCase):
    
    def setUp(self):
        logger.info('Creating test object')
        logger.info('%s' % dir(ribopy))
        logger.info('%s' % ribopy.__file__)
        self.obj = ribopy.GeneCounts()
        
    def tearDown(self):
        pass
        
    def testCreated(self):
        assert True